FROM python:3

WORKDIR /app

COPY requirements.txt ./

RUN apt-get update -y
RUN pip3 install  -r requirements.txt

COPY . .

CMD [ "python3", "-m", "flask", "run", "--host=0.0.0.0" ]

EXPOSE 5000

